#===============================================================================
# Problem Statement
# 
# Sorting
# One common task for computers is to sort data. For example, people might want 
# to see all their files on a computer sorted by size. Since sorting is a simple 
# problem with many different possible solutions, it is often used to introduce 
# the study of algorithms.
# 
# Insertion Sort
# These challenges will cover Insertion Sort, a simple and intuitive sorting
# algorithm. We will first start with an already sorted list.
# 
# Insert element into sorted list
# Given a sorted list with an unsorted number V in the right-most cell, can you
# write some simple code to insert V into the array so it remains sorted?
# 
# Print the array every time a value is shifted in the array until the array is
# fully sorted. The goal of this challenge is to follow the correct order of
# insertion sort.
# 
# Guideline: You can copy the value of V to a variable, and consider its cell
# "empty". Since this leaves an extra cell empty on the right, you can shift
# everything over until V can be inserted. This will create a duplicate of each
# value, but when you reach the right spot, you can replace a value with V.
# 
# Input Format
# There will be two lines of input:
# 
#     s - the size of the array
#     ar - the sorted array of integers
# 
# Output Format
# On each line, output the entire array every time an item is shifted in it.
# 
# Constraints
# 1<=s<=1000
# -10000<=x<= 10000, x ∈ ar
# 6 8 3
#===============================================================================
#===============================================================================
# SOLUTION
#===============================================================================
from array import array
def singleInsertionSort(arr, pos):
    # V is rightmost value
    # find value greater than V
    # copy value paste in V, paste in next val, etc
    # position to indicate where in the array to begin the sort.
    insert = arr[pos]
    for i in range(pos-1,-1,-1):
    # sorting is done from the right of the array moving left
        if arr[i] > insert :
            arr[i + 1] = arr[i]
            # print(*arr,sep=' ',end='\n')
        else :
            arr[i + 1] = insert
            # print(*arr,sep=' ',end='\n')
            return
    
    # we are now at the beginning of the list, insert into the first location
    # if we exit the for loop.    
    arr[0] = insert
    # print(*arr, sep=' ', end='\n')
    return

def insertionSort(arr):
    # Take the array and use the function singleInsertionSort to sort it
    # Do so one element at a time, and then replace the array with that new
    # array
    for q in range(1,len(arr)):
        # check if the previous value is greater than the current value
        if arr[q-1] > arr[q]:
            # sort part of the array
            singleInsertionSort(arr, q)
        # print out the newly sorted array
        print(*arr, sep=' ', end='\n')    

discard = int(input())
ar = list(map(int,input().split()))
insertionSort(ar)