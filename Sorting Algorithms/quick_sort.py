#===============================================================================
# Quicksort created for hackerrank challenge by William Marsman.
# 17/Jan/2015
#
# Unfortunately I could not get the output to match what they had for this
# sorting algorithm, although this method does appear to work.
#===============================================================================
from array import array
def emptyArrayQuickSort(arr):
# Takes an array and a pivot p, returns array with pivot inserted
# Uses two arrays to more easily sort using pivots
    smallersubarr = []
    largersubarr = []
    
    # remove p from the array
    p = arr[0]
    arr = arr[1:]
    
    # do the basic pivoting
    for element in arr:
        if element < p:
            smallersubarr.append(element);
        else :
            largersubarr.append(element);
        
    # recursively call quicksort until the smaller or larger array contains
    # only a single element, then append p, combine and print
    if len(smallersubarr) > 1 and smallersubarr != []:
        smallersubarr = QuickSort(smallersubarr)
    if len(largersubarr) > 1 and largersubarr != []:
        largersubarr = QuickSort(largersubarr)
    
    # append the pivot back to the arrays
    smallersubarr.append(p)
    # combine the arrays
    arr = smallersubarr + largersubarr
    #===========================================================================
    # For hackerrank submission
    # print(*arr, sep=' ', end='\n')
    #===========================================================================
    return arr

def quickSort(arr, lower, upper):
# Sorts the array with no extra arrays created. Instead only the original
# array plus indices can be used
# pivot is always taken as the last element
    p = arr[upper-1]
    small_ind = lower
    
    for i in range(lower, upper):
        if arr[i] <= p:
        # if the element is smaller than the pivot, swap with the bigger element
            temp = arr[i]
            arr[i] = arr[small_ind]
            arr[small_ind] = temp
            small_ind += 1;
    
    #===========================================================================
    # For hackerrank submission
    # if small_ind != upper:
    #    print(*arr, sep=' ', end='\n')
    #===========================================================================

    new_p = small_ind - 1
    if (new_p - lower > 1):
       quickSort(arr, lower, new_p)
    if (upper - 2 - new_p > 1):
       quickSort(arr, new_p, upper)

arrlen = int(input());
arr = list(map(int,input().split()))
arr = quickSort(arr, 0, arrlen)