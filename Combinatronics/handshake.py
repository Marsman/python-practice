#===============================================================================
# Problem Statement
#===============================================================================
#===============================================================================
# At the annual meeting of Board of Directors of Acme Inc, every one starts 
# shaking hands with everyone else in the room. Given the fact that any two 
# persons shake hand exactly once, Can you tell the total count of handshakes?
# 
# Input Format
# The first line contains the number of test cases T, T lines follow.
# Each line then contains an integer N, the total number of Board of Directors 
# of Acme.
# 
# Output Format
# 
# Print the number of handshakes for each test-case in a new line. 
#===============================================================================
#===============================================================================
# Solution
#===============================================================================
for _ in range(int(input())):
    # 1 persons, 0 handshakes
    # 2 persons, 1 handshakes
    # 3 persons, 2 + 1 handshakes
    # 4 persons, 3 + 2 + 1 handshakes
    # 5 persons, 4 + 3 + 2 + 1 (10) handshakes
    # how best to articulate this in a formula?
    # well it's kind of like a triangle. Maybe like that?
    # 3 * 4 / 2 = 6
    # 2 * 3 / 2 = 3
    # 1 * 2 / 2 = 1
    # 4 * 5 / 2 = 10 yea okay
    x = int(input())
    print(int(x*(x-1)/2))