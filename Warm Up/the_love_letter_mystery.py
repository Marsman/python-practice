# Problem Statement

# James found a love letter his friend Harry has written for his girlfriend. 
# James is a prankster, so he decides to meddle with the letter. He changes all 
# the words in the letter into palindromes.

# To do this, he follows 2 rules:

# (a) He can reduce the value of a letter, e.g. he can change 'd' to 'c', but 
# he cannot change 'c' to 'd'.
# (b) In order to form a palindrome, if he has to repeatedly reduce the value 
# of a letter, he can do it until the letter becomes 'a'. Once a letter has 
# been changed to 'a', it can no longer be changed.

# Each reduction in the value of any letter is counted as a single operation. 
# Find the minimum number of operations required to convert a given string into 
# a palindrome.

# Input Format
# The first line contains an integer T, i.e., the number of test cases.
# The next T lines will contain a string each. The strings do not contain any 
# spaces.

# Output Format
# A single line containing the number of minimum operations corresponding to
# each test case.

# Constraints
# 1 ≤ T ≤ 10
# 1 ≤ length of string ≤ 104
# All characters are lower case English letters. 

#----------#
# SOLUTION #
#----------#
# Solution by Lorne 'William' Marsman
#
# single palindrome solution runs in n time

# number of times to run this
for _ in range(int(input())):
    # for each string, determine the distance between each letter
    # ie a - d = 3, 3 reductions til complete
    palenstr = input()
    dist = 0
    for ind in range(int(len(palenstr)/2)):
        dist += abs(ord(palenstr[ind]) - ord(palenstr[-(ind+1)]))
        
    print(dist)
    
#------------------#
# TESTCASE RESULTS #
#------------------#

# 0 0.05s : Success (sample)
# 1 0.05s : Success
# 2 0.05s : Success
# 3 0.05s : Success
# 4 0.04s : Success
# 5 0.05s : Success
# 6 0.05s : Success
# 7 0.07s : Success
# 8 0.07s : Success (additional)
# 9 0.07s : Success (additional)
# 10 0.07s : Success (additional)
# 11 0.04s : Success (additional)
