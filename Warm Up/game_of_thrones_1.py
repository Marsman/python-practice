#===============================================================================
# Problem Statement
#===============================================================================
#===============================================================================
# King Robert has 7 kingdoms under his rule. He finds out from a raven that the 
# Dothraki are soon going to wage a war against him. But, he knows the Dothraki
# need to cross the Narrow River to enter his realm. There is only one bridge
# that connects both banks of the river which is sealed by a huge door.
# 
# The king wants to lock the door so that the Dothraki can't enter. To lock
# the door he needs a key that is an anagram of a certain palindrome string.
# 
# The king has a string composed of lowercase English letters. Help him 
# figure out if any anagram of the string can be a palindrome or not.
# 
# Input Format
# A single line which contains the input string
# 
# Constraints
# 1<=length of string <= 10^5
# Each character of the string is a lowercase English letter.
# aaaccccdddd
# Output Format
# A single line which contains YES or NO in uppercase.
#===============================================================================
#===============================================================================
# SOLUTION
#===============================================================================
#===============================================================================
# requirements for a string to have a palindrome permutation 
# one string of characters must have an odd amount
# remaining strings of characters must have even amounts
# if the string is sorted, and then the different types of each character are 
# counted, and 1 odd string or 0 is found with N even strings, it's a palendrone
# so if we could two odd strings we're done
#
# to do that just check if the first character is equal to the second character
#===============================================================================
from test.regrtest import PASSED

# get string
palinstring = sorted(input())
oddcounter = 0
adjust = 0

# make it even
if len(palinstring)%2 != 0 :
    palinstring.append('')
    
# since we know it is even, no need to worry about invalid ranges
for ind in range(0,len(palinstring),2) :
    if palinstring[ind - adjust] != palinstring[ind-adjust+1] :
        # reduce index by one via adjust to continue along the 'even line'
        # e.g. 
        # a, a, >a, >b, b, b, b, e
        # to
        # a, a, a, >b, >b, b, b, e
        # instead of
        # a, a, a, b, >b, >b, b, e
        adjust += 1

# check to see if we hit 2 odd things. if so bounce outta here
if adjust > 1 :
    print("NO")
else :
    print("YES")
#===============================================================================
# RESULTS
#===============================================================================
# 0 0.05s : Success (sample)
# 1 0.05s : Success (sample)
# 2 0.05s : Success (sample)
# 3 0.1s : Success
# 4 0.1s : Success
# 5 0.1s : Success
# 6 0.1s : Success
# 7 0.1s : Success
# 8 0.1s : Success
# 9 0.1s : Success
# 10 0.1s : Success
# 11 0.1s : Success
# 12 0.1s : Success
# 13 0.1s : Success
# 14 0.1s : Success
# 15 0.1s : Success
# 16 0.1s : Success
# 17 0.1s : Success
# 18 0.1s : Success
# 19 0.1s : Success
# 20 0.1s : Success
