# Problem Statement

# Bill Gates is on one of his philanthropic journeys to a village in Utopia. He 
# has N packets of candies and would like to distribute one packet to each of the
# K children in the village (each packet may contain different number of candies) 
# To avoid any fighting among the children, he would like to pick K out of N 
# packets, such that unfairness is minimized.

# Suppose the K packets have (x1, x2, x3,....xk) candies in them, where xi 
# denotes the number of candies in the ith packet, then we define unfairness as

# max(x1,x2,...xk) - min(x1,x2,...xk)

# where max denotes the highest value amongst the elements, and min denotes the 
# least value amongst the elements. Can you figure out the minimum unfairness and 
# print it?

# Input Format
# The first line contains an integer N.
# The second line contains an integer K. 
# N lines follow. 
# Each line contains an integer that denotes the amount of candy in that packet.

# Output Format
# An integer that denotes the minimum possible value of unfairness.

# Constraints
# 1<=N<=105
# 1<=K<=N
# 0<= number of candies in any packet <=109

#----------#
# SOLUTION #
#----------#
# Solution By Lorne 'William' Marsman
# I want to the closest two numbers with K - 2 numbers between them
# For each number, go K - 1 indices forward, find the difference
# If the difference is greater than the current min, I want to save the upper 
# and lower
# Do this for each number against each other number
# Must somehow address scenario with only a single element
#   Special case? Or is generic solution possible
#   This sol happens to already handle the special case, if N = 1 and K = 1 
#   then unfairness = 0 as i = j and packets[i] - packets[j] = 0
#
#   runs in n log(n)

N = int(input())
K = int(input())

# acquire the packet list 
packets = []
for _ in range(N):
    packets.append(int(input()))

# sort the packet list, to make comparing sizes much easier
packets.sort()

# now check the unfairness between the smallest and K - 1 bigger 
unfairness = float('inf')
for i in range(N - (K - 1)):
    j = (i + (K - 1))
    temp = packets[j] - packets[i]
    if unfairness > temp:
        unfairness = temp
        
print(unfairness)

#------------------#
# TESTCARE RESULTS #
#------------------#

# 0 0.05s : Success (sample)
# 1 0.05s : Success (sample)
# 2 0.05s : Success
# 3 0.05s : Success
# 4 0.06s : Success
# 5 0.05s : Success
# 6 0.09s : Success
# 7 0.46s : Success
# 8 0.43s : Success
# 9 0.43s : Success
# 10 0.38s : Success
# 11 0.44s : Success
# 12 0.36s : Success
# 13 0.42s : Success
# 14 0.35s : Success
# 15 0.05s : Success