#===============================================================================
# Problem Statement
#===============================================================================
#===============================================================================
# Given an array A={a1,a2,…,aN} of N elements, find the maximum possible sum of
# 
#     1. a Contiguous subarray
#     2. a Non-contiguous (not necessarily contiguous) subarray.
# 
# Empty subarrays/subsequences should not be considered. 
#===============================================================================
#===============================================================================
# Solution
#===============================================================================
def max_subarray(arr):
# Find both the contiguous and non-contiguous maximum arrays. Empty arrays are
# not allowed.
# For non-contiguous, the maximum is either the smallest negative number or the
# all positive numbers added together
# For contiguous, the maximum is found through the use of Kadanes algorithm
    max_noncontig = max_ending_here = max_so_far = arr[0];
    for x in arr[1:]:
        # find out if the maximum is greater including x or if x is the first
        max_ending_here = max(x, max_ending_here + x)
        # this is for non-contiguous
        max_noncontig = max(x, max_noncontig + x, max_noncontig)
        # find out if this is greater than the maximum sub array found so far
        max_so_far = max(max_ending_here, max_so_far)
    return max_so_far, max_noncontig

number_of_cases = int(input())
for _ in range(number_of_cases):
    input()
    out = max_subarray(list(map(int,input().split())))
    print(out[0], out[1],sep=" ", end="\n")